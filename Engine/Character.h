#pragma once
#include "Vec2D.h"
#include "Keyboard.h"
#include "Graphics.h"

class Character
{
public:
	Character() = default;
	Character(Vec2D pos_in, Vec2D vel_in);
	void ClampToScreen();
	void Update(const Keyboard& kbd, const float dt);
	void UpdateVelocity(const float dt);
	void UpdatePosition(const float dt);
	void GiveMomentum(const Vec2D& momentum);
	void Draw(Graphics& gfx);
	Vec2D GetVelocity();

// Member Variables
private:
	bool jumping = false;
	bool movingLeft = false;
	bool movingRight = false;
	bool onGround = false;
	Vec2D pos;
	Vec2D vel;
	Vec2D acc = Vec2D::Null();
	Vec2D grav = Vec2D(0.0f, gravityAcc);

// Core Class Parameters
private:
	static constexpr float gravityAcc = 1500.0f;
	static constexpr float walkAcc = 1000.0f;
	static constexpr float frictionAcc = 3000.0f;
	static constexpr float jumpDamping = 1.0f;
	static constexpr float jumpingSpeed = 800.0f;
	static constexpr float walkSpeed = 300.0f;
	static constexpr float width = 30.0f;
	static constexpr float height = 30.0f;

};

