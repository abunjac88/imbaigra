#include "Vec2D.h"
#include <cmath>

Vec2D::Vec2D(float x_in, float y_in)
	:
	x(x_in),
	y(y_in)
{
}

Vec2D Vec2D::operator+(const Vec2D& rhs) const
{
	return Vec2D(x + rhs.x, y + rhs.y);
}

Vec2D& Vec2D::operator+=(const Vec2D& rhs)
{
	return *this = *this + rhs;
}

Vec2D Vec2D::operator-(const Vec2D& rhs) const
{
	return Vec2D( x - rhs.x, y - rhs.y );
}

Vec2D& Vec2D::operator-=(const Vec2D& rhs)
{
	return *this = *this - rhs;
}

Vec2D Vec2D::operator*(const float rhs) const
{
	return Vec2D( x *rhs, y *rhs );
}

Vec2D& Vec2D::operator*=(const float rhs)
{
	return *this = *this * rhs;
}

float Vec2D::GetX() const
{
	return x;
}

float Vec2D::GetY() const
{
	return y;
}

float Vec2D::GetLengthSquared() const
{
	return x*x + y*y;
}

float Vec2D::GetLength() const
{
	return std::sqrt( GetLengthSquared() );
}

Vec2D Vec2D::GetNormalized() const
{
	const float length = GetLength();
	if (length != 0.0f) 
	{
		return *this * (1.0f / length);
	}
	return *this;
}

Vec2D& Vec2D::Normalize()
{
	return *this = GetNormalized();
}

Vec2D Vec2D::Left()
{
	return Vec2D(-1.0f,0.0f);
}

Vec2D Vec2D::Right()
{
	return Vec2D(1.0f,0.0f);
}

Vec2D Vec2D::Up()
{
	return Vec2D(0.0f,-1.0f);
}

Vec2D Vec2D::Down()
{
	return Vec2D(0.0f,1.0f);
}

Vec2D Vec2D::Null()
{
	return Vec2D(0.0f,0.0f);
}


