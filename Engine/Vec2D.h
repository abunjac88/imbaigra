#pragma once
class Vec2D
{
public:
	Vec2D() = default;
	Vec2D(float x_in, float y_in);
	Vec2D operator+(const Vec2D& rhs) const;
	Vec2D& operator+=(const Vec2D& rhs);
	Vec2D operator-(const Vec2D& rhs) const;
	Vec2D& operator-=(const Vec2D& rhs);
	Vec2D operator*(const float rhs) const;
	Vec2D& operator*=(const float rhs);
	float GetX() const;
	float GetY() const;
	float GetLengthSquared() const;
	float GetLength() const;
	Vec2D GetNormalized() const;
	Vec2D& Normalize();
public:
	static Vec2D Left();
	static Vec2D Right();
	static Vec2D Up();
	static Vec2D Down();
	static Vec2D Null();
public:
	float x;
	float y;
};

