#include "Character.h"
#include "Colors.h"

Character::Character(Vec2D pos_in, Vec2D vel_in)
	:
	pos(pos_in),
	vel(vel_in)
{
}

void Character::ClampToScreen()
{
	const float left = pos.GetX() - width/2;
	const float right = pos.GetX() + width/2;
	if (left < 0)
	{
		pos.x = width/2;
		vel.x = 0;
		acc.x = 0;
	}
	else if (right >= float(Graphics::ScreenWidth))
	{
		pos.x = float(Graphics::ScreenWidth - 1) - width/2;
		vel.x = 0;
		acc.x = 0;
	}

	const float bottom = pos.GetY() + height/2;
	const float top = pos.GetY() - height/2;
	if (top < 0)
	{
		pos.y = height/2;
		vel.x = 0;
		acc.x = 0;
	}
	else if (bottom >= float(Graphics::ScreenHeight))
	{
		pos.y = float(Graphics::ScreenHeight - 1) - height/2;
		vel.x = 0;
		acc.x = 0;
	}
}

void Character::Update(const Keyboard& kbd, const float dt)
{
	//resolves the LEFT KEY input
	if (kbd.KeyIsPressed(VK_LEFT))
	{
		if (!jumping) 
		{
			if (std::abs(vel.x) < walkSpeed) acc.x = -walkAcc;
			movingLeft = true;
		}
		else if (movingLeft)
		{
			acc.x = 0;
			movingLeft = true;
		}
	}
	else if (movingLeft && !jumping)
	{
		if (vel.x < 0)
		{
			acc.x = frictionAcc;
		}
		else
		{
			movingLeft = false;
			acc.x = 0.0f;
			vel.x = 0.0f;
		}
	}
	else if (movingLeft)
	{
		acc.x = 0.0f;
	}

	// resolves the RIGHT key input
	if (kbd.KeyIsPressed(VK_RIGHT))
	{
		if (!jumping) {
			if (std::abs(vel.x) < walkSpeed) acc.x = walkAcc;
			movingRight = true;
		}
		else if (movingRight)
		{
			acc.x = 0;
			movingRight = true;
		}
	}
	else if (movingRight && !jumping)
	{
		if (vel.x > 0)
		{
			acc.x = -frictionAcc;
		}
		else
		{
			movingRight = false;
			vel.x = 0.0f;
			acc.x = 0.0f;
		}
	}
	else if (movingRight)
	{
		acc.x = 0.0f;
	}

	// give directional momentum when pressing the jump buttom
	if (kbd.KeyIsPressed(VK_UP) && !jumping)
	{
		Vec2D jumpMomentum = Vec2D(0.0f, -jumpingSpeed) + vel;
		vel = Vec2D(0.0f, 0.0f);
		jumpMomentum = jumpMomentum.GetNormalized() * jumpingSpeed;
		GiveMomentum( jumpMomentum );
		jumping = true;
	}

	// stop jumping when reaching the ground
	if ( jumping && int(pos.GetY() + height / 2) >= Graphics::ScreenHeight - 1 && int(vel.GetY()) > 0 )
	{
		jumping = false;
		vel.y = 0.0f;
	}

	// update position and velocity for the next frame
	UpdatePosition(dt);
	UpdateVelocity(dt);
}

void Character::UpdateVelocity(const float dt)
{
	if (jumping) {
		vel += (acc + grav) * dt;
	}
	else
	{
		vel += acc * dt;
	}
}

void Character::UpdatePosition(const float dt)
{
	pos += vel * dt;
}

void Character::GiveMomentum(const Vec2D& momentum)
{
	vel += momentum;
}

void Character::Draw(Graphics& gfx)
{
	gfx.DrawRect(int( pos.GetX() - width/2 ), int( pos.GetY() - height / 2 ),int( pos.GetX() + width / 2 ), int( pos.GetY() + height / 2 ), Colors::Cyan );
}

Vec2D Character::GetVelocity()
{
	return vel;
}
